﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HeapSorting
{
    public interface IValue
    {
        int Value { get; set; }
    }

    public class Item : IComparable<Item>, IValue
    {
        public int Value { get; set; }

        public int CompareTo(Item other)
        {
            if (ReferenceEquals(this, other)) return 0;
            if (ReferenceEquals(null, other)) return 1;
            return Value.CompareTo(other.Value);
        }
    }

    public class Items<T> where T : IComparable<T>, IValue
    {
        private IList<T> _items = new List<T>();

        public Items(IList<T> list)
        {
            if (list == null)
                throw new NullReferenceException();

            if (list.Count == 0)
                throw new ArgumentException("list must have more than once");

            foreach (var item in list)
            {
                _items.Add(item);
            }
        }

        public void HeapSort()
        {
            int listLen = _items.Count;

            for (int i = listLen / 2; i >= 0; i--)
            {
                Heapify(_items, listLen, i);
            }

            for (int i = listLen - 1; i >= 0; i--)
            {
                T temp = _items[0];
                _items[0] = _items[i];
                _items[i] = temp;

                Heapify(_items, i, 0);
            }
        }

        private void Heapify(IList<T> items, int listLen, int i)
        {
            int largestIndex = i;
            int leftIndex = 2 * i + 1;
            int rightIndex = 2 * i + 2;

            if (leftIndex < listLen && _items[leftIndex].CompareTo(_items[largestIndex]) > 0)
            {
                largestIndex = leftIndex;
            }

            if (rightIndex < listLen && _items[rightIndex].CompareTo(_items[largestIndex]) > 0)
            {
                largestIndex = rightIndex;
            }

            if (largestIndex != i)
            {
                T temp = _items[i];
                _items[i] = _items[largestIndex];
                _items[largestIndex] = temp;

                Heapify(_items, listLen, largestIndex);
            }
        }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            foreach (var item in _items)
            {
                stringBuilder.Append(item.Value).Append("-");
            }

            return stringBuilder.ToString();
        }
    }

    public class Items : Items<Item>
    {
        public Items(IList<Item> list) : base(list)
        {
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            List<Item> items = new List<Item>()
            {
                new Item() {Value = 34},
                new Item() {Value = 23},
                new Item() {Value = 65},
                new Item() {Value = 76},
                new Item() {Value = 87},
                new Item() {Value = 9},
                new Item() {Value = 12},
            };

            Items itemList = new Items(items);
            itemList.HeapSort();
            Console.WriteLine(itemList);
        }
    }
}